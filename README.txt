
-- SUMMARY --

Contextual Block Hide allows easily administors with rights to hide a block directly in the current page with the contextual links.


-- REQUIRENENTS --


-- INSTALLATION --

1. Extract cbh on your module directory (ex. sites/all/modules)
2. Enable the module in Modules panel
3. Enjoy

-- CONFIGURATION --

No configuration is required

-- TROUBLESHOOTING --

-- FAQ --

-- CONTACT --

Current maintainers:
* Yvan Marques (yvmarques) - http://drupal.org/user/298685